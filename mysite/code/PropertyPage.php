<?php
class PropertyPage extends Page{
    private static $has_one = array(
        'Properties' => 'PropertyData'
    );
}

class PropertyPage_Controller extends Page_Controller{
    private static $allowed_actions = array(
        'test',
        'detail'
    );

    public function test(){
        die('it works');
    }

    // function index(){
        
    // }

    
    public function detail(SS_HTTPRequest $request){
        $property = PropertyData::get()->filter(array(
            'UrlSegment' => $request->param('ID')
        ))->first();

        if(!$property){
            return $this->httpError(404, 'That region could not be found');
        }
        
        $wanumber = GlobalFunction::GenerateWaNumber($property->Agent()->NoHp);

        // Debug::show($agentPP);
        // Debug::show($propertyphoto);
        // die();

        $data = array(
            'Property' => $property,
            'Title' => $property->Title,
            'AgentName' => $property->Agent()->Name,
            'AgentPhoto' => $property->Agent()->ProfilePicture(),
            'NoHp' => $property->Agent()->NoHp,
            'WhatsAppNumber' => $wanumber
        );

        // Debug::show($data);
        // die();

        return $data;
    }
}
?>