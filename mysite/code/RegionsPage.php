<?php
class RegionsPage extends Page {
    private static $has_many = array (
        'Regions' => 'RegionData'
    );
    public function getCMSFields() {
        // echo die;
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Regions', GridField::create(
            'Regions',
            'Regions on this page',
            $this->Regions(),
            GridFieldConfig_RecordEditor::create()
        ));

        return $fields;
    }
}

class RegionsPage_Controller extends Page_Controller {
    //start lesson 14
    private static $allowed_actions = array(
        //'test',
        'show'
    );

    public function test(){
        die('it works');
    }

    public function show(SS_HTTPRequest $request){

        //$region = RegionData::get()->byID($request->param('ID'));
        $region = RegionData::get()->filter(array(
            'UrlSegment' => $request->param('ID')
        ))->first();

        if(!$region){
            return $this->httpError(404, 'That region could not be found');
        }

        // var_dump($region);
        // die();
        return array(
            'Region' => $region,
            'Title' => $region->Title,
        );
    }

    //end lesson 14
}
?>