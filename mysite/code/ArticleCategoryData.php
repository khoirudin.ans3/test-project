<?php

class ArticleCategoryData extends DataObject{
    private static $db = array(
        'Title' => 'Varchar',
        'UrlSegment' => 'Varchar'
    );

    private static $has_one = array(
        'ArticleHolderPage' => 'ArticleHolderPage',
    );

    private static $belongs_many_many = array(
        'Articles' => 'ArticlePage',
    );

    //startlesson19
    public function Link() {
        // $urlsegment = str_replace(' ', '-', $this->Title);
        return $this->ArticleHolderPage()->Link(
            'category/'.$this->UrlSegment
        );
    }
    //endlesson19

    public function getCMSFields(){
        return Fieldlist::create(
            TextField::create('Title')
        );
    }

    // public function onAfterWrite(){
    //     parent::onAfterWrite();
    //     // use the find() method to look up the relation
    //     $item = ArticleCategoryData::get()->find('ID', $this->ID);
    //     $urlnew = str_replace(' ', '-', $item->Title);
    //     $urlnew = strtolower($urlnew);
        
    //     // $urldb = ArticleCategoryData::get()->filter(array(
    //     //     'UrlSegment' => $urlnew
    //     // ))->first();
        
        
    //     $jmlhSameCategory = ArticleCategoryData::get()->filter(array(
    //         'Title' => $item->Title
    //     ))->count();

    //     $extraUrl = $jmlhSameCategory;
        
    //     if($jmlhSameCategory == 1){
    //         $item->UrlSegment = $urlnew;
    //         $item->write();
    //     }
    //     // if($jmlhSameCategory == 2){
    //     //     $item->UrlSegment = $urlnew.' '. $jmlhSameCategory;
    //     //     $item->write();
    //     // }
    //     if($jmlhSameCategory>=2){
    //         // $item->UrlSegment = $urlnew.' '.$jmlhSameCategory;
    //         // $item->write();
    //         //$jmlhNew = $jmlhSameCategory-1;
    //         $i = 0;
    //         while($i <= $jmlhSameCategory){
    //             if ($i == $jmlhSameCategory){
    //                 $item->UrlSegment = $urlnew.'-'.$extraUrl;
    //                 $item->write();
    //             }
    //             $i++;
    //         }
    //     }
        
    //     // while($i <= $jmlhNew){
    //     //     $i++;
    //     //     if ($i == $jmlhNew){
    //     //         $item->UrlSegment = $urlnew.'-'.$i++;
    //     //         $item->write();
    //     //     }

    //     // }
        
    //     // while($urldb->UrlSegment == $urlnew){
    //     //     $urlnew = $urlnew;
    //     //     $item->UrlSegment = $urlnew.'-1';
    //     //     $item->write();
    //     //     break;
    //     // }
    //     // check that the related item exists before editing
    //     // if($item){
    //     //     $urlsegment = str_replace(' ', '-', $item->Title);
    //     //     $urlsegment = strtolower($urlsegment);
    //     //     $item->UrlSegment = $urlsegment;
    //     //     $item->write();
    //     // }
    // }

    public function onBeforeWrite(){
        parent::onBeforeWrite();
        $item = $this;
        $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
        $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegment);
        // Debug::show($findurlsegment);
        // $findurlsegment = true;
        if($findurlsegment){
            // Debug::show('==========================================ada');
            $i=0;
            while($findurlsegment){
                // $urlsegmenextra = $findurlsegment->UrlSegment.'-'.$i++;
                // $urlsegment = GlobalFunction::GenerateUrlSegment($findurlsegment);
                
                $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i);
                $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
                if(!$findurlsegment){
                    // Debug::show('==========================================loop');
                    $item->UrlSegment = $urlsegmentdb;
                    //return $findurlsegment;
                    // return true;
                }
                $i++;
            }
        }
        else{
            // Debug::show('==========================================gada');
            $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
            $item->UrlSegment = $urlsegment;
            // Debug::show('==========================================gada  '.$item->UrlSegment);
            // return true;
        }
        // $i = 0;
        // while($findurlsegment){
        //     // $urlsegmenextra = $findurlsegment->UrlSegment.'-'.$i++;
        //     // $urlsegment = GlobalFunction::GenerateUrlSegment($findurlsegment);
        //     // $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i);
        //     // $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
        //     $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title);
        //     $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
        //     Debug::show('<br>==================='.$urlsegmentdb);
        //     Debug::show('<br>==================='.$findurlsegment);
        //     if($i == 0){
        //         $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title);
        //         $item->UrlSegment = $urlsegmentdb;
        //         $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
        //         Debug::show('<br>===================if'.$i.''.$findurlsegment);

        //     }
        //     else{
                
        //         $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i); 
        //         $item->UrlSegment = $urlsegmentdb;
        //         $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
        //         Debug::show('<br>===================else'.$i.''.$findurlsegment);

        //     }
        //     // // $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i);
        //     // $findurlsegment = GlobalFunction::FindUrlSegmentCategory($urlsegmentdb);
        //     // Debug::show('<br>===================else '.$urlsegmentdb);
        //     // $item->UrlSegment = $urlsegmentdb;
        //     // // Debug::show('<br>==================='.$item);
        //     $i++;
        //     die();
        // }

        // die();
    }
    public function onAfterWrite(){
        parent::onAfterWrite();
        $item = ArticleCategoryData::get()->find('ID', $this->ID);
        // Debug::show('==========================================afterwrite '.$this->UrlSegment);
        $urlsegment = GlobalFunction::GenerateUrlSegment($item->Title);
        // Debug::show('<br>==========================================urlsegment '.$urlsegment.'-0');
        if($item->UrlSegment == $urlsegment.'-0'){
            $item->UrlSegment = $urlsegment;
            $item->write();
            
        }
    }
}

?>