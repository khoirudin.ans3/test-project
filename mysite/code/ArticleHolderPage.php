<?php
class ArticleHolderPage extends Page {
    private static $allowed_children = array (
		'ArticlePage'
	);

	private static $has_many = array(
		'Categories' => 'ArticleCategoryData',
		'Tags' => 'ArticleTagData'
	);

	public function Regions() {
		$page = RegionsPage::get()->first();

		if($page){
			return $page->Regions();
		}
	}

	public function  getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->addFieldToTab('Root.Categories', GridField::create(
			'Categories',
			'Article categories',
			$this->Categories(),
			GridFieldConfig_RecordEditor::create()
		));
		$fields->addFieldToTab('Root.Tags', GridField::create(
			'Tags',
			'Article Tags',
			$this->Tags(),
			GridFieldConfig_RecordEditor::create()
		));

		return $fields;
	}

	//start lesson 20
	public function ArchiveDates() {
		$list = ArrayList::create();
        $stage = Versioned::current_stage();

        $query = new SQLQuery(array ());
        $query->
				selectField("DATE_FORMAT(`Date`,'%Y_%M_%m')","DateString")
				// selectField("Date")
            //   ->setFrom("ArticlePage_{$stage}")
			  ->setFrom("ArticlePage")
              ->setOrderBy("DateString", "ASC")
              ->setDistinct(true);

        $result = $query->execute();

        if($result) {
            while($record = $result->nextRecord()) {
				list($year, $monthName, $monthNumber) = explode('_', $record['DateString']);
				// list($year, $monthName, $monthNumber) =  $record['Date'];

                $list->push(ArrayData::create(array(
                    'Year' => $year,
                    'MonthName' => $monthName,
                    'MonthNumber' => $monthNumber,
                    'Link' => $this->Link("date/$year-$monthName"),
                    'ArticleCount' => ArticlePage::get()->where("
                            DATE_FORMAT(`Date`,'%Y%m') = '{$year}{$monthNumber}'
                            AND ParentID = {$this->ID}
                        ")->count()
                )));
            }
        }

		return $list;
	}

	//end lesson 20
}

class ArticleHolderPage_Controller extends Page_Controller {
	//start lesson 19
	private static $allowed_actions = array(
		'category',
		'region',
		'date'
	);

	protected $articleList;
	
	public function init() {
		parent::init();
		$this->articleList = ArticlePage::get()->filter(array(
			'ParentID' => $this->ID
		))->sort('Date DESC');
	}

	// public function PaginatedArticles($num = 10) {
	// 	return PaginatedList::create(
	// 		$this->articleList,
	// 		$this->getRequest()
	// 	)->setPageLength($num);
	// }
	
	public function index (SS_HTTPRequest $request){
		$articles = ArticlePage::get();
		$search = $request->getVar('Teaser');
		$region = $request->getVar('Region');
		$category = $request->getVar('Category');
		$sqlwhere = "";
		if($search){
			$sqlwhere = " $sqlwhere AND ap.Teaser Like '%$search%' ";
		}

		if($region){
			$sqlwhere = " $sqlwhere AND ap.RegionID = '$region'";
		}
		if($category){
			$category = implode("','", $category);
			$sqlwhere = " $sqlwhere AND ArticleCategoryDataID IN ('$category')";
			// $query = DB::query("SELECT * FROM ArticlePage_Categories WHERE ArticleCategoryDataID IN ('$category')");
			// $newcategorylist = new ArrayList();
			// foreach($query as $q){
			// 	$articlePageID = $q['ArticlePageID'];
			// 	$articlespage = $articles->filter(array(
			// 		'ID' => $articlePageID
					
			// 	))->first();
			// 	$newcategorylist->push($articlespage);
			// }
			// $articles = $newcategorylist;
		}

		$sqlquery = "SELECT ap.ID as ArticlePageID , ap.Teaser
		FROM ArticlePage ap LEFT JOIN ArticlePage_Categories apc ON ap.ID = apc.ArticlePageID
		where ap.Teaser != '' $sqlwhere Group by ArticlePageID";
		$query = DB::query($sqlquery);
		$newarticlesdata = new ArrayList();
		foreach($query as $q){
			$article = ArticlePage::get();
			$articlepageid = $q['ArticlePageID'];
			$articleList = $article->filter(array(
				'ID' => $articlepageid
			))->first();
			$newarticlesdata->push($articleList);
		}
		$articles = $newarticlesdata;
		
		$paginatedArticles = PaginatedList::create(
			$articles,
			$request
		);
	
		$data =  array(
			'Results' => $paginatedArticles
		);

        return $data;

	}


	public function category (SS_HTTPRequest $r) {
		// $category = ArticleCategoryData::get()->byID(
		// 	$r->param('ID')
		// );
		$category = ArticleCategoryData::get()->filter(array(
            'UrlSegment' => $r->param('ID')
        ))->first();
		
		if(!$category){
			return $this->httpError(404, 'That category not found');
		}

		$this->articleList = $this->articleList->filter(array(
			'Categories.ID' => $category->ID
		));

		// return array(
		// 	'SelectedCategory' => $category
		// );
		$paginatedArticles = PaginatedList::create(
			$this->articleList
		);
	
		$data =  array(
			'Results' => $paginatedArticles
		);

		return $data;
	}

	public function region (SS_HTTPRequest $r) {
		// $region = RegionData::get()->byID(
		// 	$r->param('ID')
		// );
		$region = RegionData::get()->filter(array(
            'UrlSegment' => $r->param('ID')
        ))->first();

		if(!$region){
			return $this->httpError(404, 'That region not found');
		}

		$this->articleList = $this->articleList->filter(array(
			'RegionID' => $region->ID
		));

		// Debug::show($this->articleList);
		// die();

		// return array(
		// 	'SelectedRegion' => $region
		// );
		$paginatedArticles = PaginatedList::create(
			$this->articleList
		);
	
		$data =  array(
			'Results' => $paginatedArticles
		);

        return $data;
	}
	//end lesson 19

	//start lesson 20
	public function date(SS_HTTPRequest $r) {
		$year = $r->param('ID');
		$month = $r->param('OtherID');
		if(!$year) return $this->httpError(404);

		$startDate = $month? "{$year}-{$month}-1" : "{$year}-01-01";

		if(strtotime($startDate) === false){
			return $this->httpError(404, 'Invalid date');
		}
		$newStartDate = date('Y-m-d', strtotime(
			$startDate,
			strtotime($startDate)
		));
		//$adder = $month ? '+1 month' : '+1 year';
		$adder = $month = '+1 month';
		$endDate = date('Y-m-d', strtotime(
						$adder,
						strtotime($startDate)
					));
					
		$this->articleList = $this->articleList->filter(array(
			'Date:GreaterThanOrEqual' => $newStartDate,
			'Date:LessThan' => $endDate
		));
		
		// Debug::show($month);
		// Debug::show($newStartDate);
		// Debug::show($startDate);
		// Debug::show($endDate);

		// return array(
		// 	'StartDate' => DBField::create_field('SS_DateTime', $startDate),
		// 	'EndDate' => DBField::create_field('SS_DateTime', $endDate)
		// );
		$paginatedArticles = PaginatedList::create(
			$this->articleList
		);

		// Debug::show($this->articleList);
		// die();
		$data =  array(
			'Results' => $paginatedArticles,
			'StartDate' => DBField::create_field('SS_DateTime', $newStartDate),
			'EndDate' => DBField::create_field('SS_DateTime', $endDate)
		);

        return $data;
	}
	//end lesson 20


	public function ArticleSearchForm(){
		// $list = ArrayList::create();
		// $query = new SQLQuery(array ());
        // $query->
		// 		selectField("DATE_FORMAT(`Date`,'%Y_%M_%m')","DateString")
		// 		// selectField("Date")
        //     //   ->setFrom("ArticlePage_{$stage}")
		// 	  ->setFrom("ArticlePage")
        //       ->setOrderBy("DateString", "ASC")
        //       ->setDistinct(true);

        // $result = $query->execute();
		// $newyearmonth = new ArrayList();
		// foreach($result as $q){
		// 	$year = $q['DateString'];
		// 	list($year, $monthName, $monthNumber) = explode('_', $q['DateString']);
		// 	$monthYear = ($monthName.' '.$year);
		// 	// Debug::show($monthYear);
		// 	$newyearmonth->push($monthYear);
		// }
		// $year = $newyearmonth;
		
		$form = Form::create(
			$this,
			'ArticleSearchForm',
			FieldList::create(
				TextField::create('Teaser')
					->setAttribute('placeholder', 'Teaser article')
					->addExtraClass('form-control'),
				DropdownField::create('Region', 'Region')
					->setEmptyString('--any--')
					->setSource(
						RegionData::get()->map('ID', 'Title')
					)
					->addExtraClass('form-control'),
				// DropdownFIeld::create('Year', 'Year')
                //     ->setEmptyString('-- any --')
                //     ->setSource($year)
                //     ->addExtraClass('form-control'),
				CheckboxSetField::create('Category', 'Category')
					->setSource(
						ArticleCategoryData::get()->map('ID', 'Title')
					)
					->addExtraClass('form-check')
			),


			FieldList::create(
				FormAction::create('doArticleSearch', 'Search')
					->addExtraClass('btn-lg btn-fullcolor')
			)
		);

		$form->setFormMethod('GET')
			 ->setFormAction($this->Link())
			 ->disableSecurityToken()
			 ->loadDataFrom($this->request->getVars());
			
		return $form;
	}
}
?>