<?php
class RegionData extends DataObject{
    private static $db = array(
        'Title' => 'Varchar',
        'Description' => 'HTMLText',
        'UrlSegment' => 'Varchar'
    );

    private static $has_one = array(
        'Photo' => 'Image',
        'RegionsPage' => 'RegionsPage'
    );

    private static $has_many = array(
        'Articles' => 'ArticlaPage'
    );

    private static $summary_fields = array(
        'GridThumbnail' => '',
        'Photo.Filename' => 'Photo file name',
        'Title' => 'Title of region',
        'Description' => 'Short description',
        'UrlSegment' => 'Url Segment'
    );

    public function getGridThumbnail() {
        if($this->Photo()->exists()) {
            return $this->Photo()->SetWidth(100);
        }

        return "(no image)";
    }

    public function getCMSFields(){
        $fields = FieldList::create(
            TextField::create('Title'),
            HtmlEditorField::create('Description'),
            $uploader = UploadField::create('Photo')
        );

        $uploader->setFolderName('region-photos');
        $uploader->getValidator()->setAllowedExtensions(array('png','gif', 'jpeg', 'jpg'));

        return $fields;
    }

    public function onBeforeWrite(){
        parent::onBeforeWrite();
        
        $item = $this;
        //convert
        $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
        $findurlsegment = GlobalFunction::FindUrlSegmentRegion($urlsegment);
        // Debug::show($findurlsegment);
        if($findurlsegment){
            $i=0;
            while($findurlsegment){
                // $urlsegmenextra = $findurlsegment->UrlSegment.'-'.$i++;
                // $urlsegment = GlobalFunction::GenerateUrlSegment($findurlsegment);
                
                $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i);
                $findurlsegment = GlobalFunction::FindUrlSegmentRegion($urlsegmentdb);
                if(!$findurlsegment){
                    $item->UrlSegment = $urlsegmentdb;
                }
                $i++;
            }
        }
        else{
            
            $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
            $item->UrlSegment = $urlsegment;
        }

        // $urlnew = str_replace(' ', '-', $item->Title);
        // $urlnew = strtolower($urlnew);
        // $countregion = RegionData::get()->filter(array(
        //     'Title' => $item->Title
        // ))->count();
        // if($countregion == 1){
        //     $item->UrlSegment = $urlnew;
        //     $item->write();
        // }
        // if($countregion>=2){
        //     $i = 0;
        //     while($i <= $countregion){
        //         if ($i == $countregion){
        //             $item->UrlSegment = $urlnew.'-'.$i++;
        //             $item->write();
        //         }
        //         $i++;
        //     }
        // }

    }

    public function onAfterWrite(){
        parent::onAfterWrite();
        $item = RegionData::get()->find('ID', $this->ID);
        // Debug::show('==========================================afterwrite '.$this->UrlSegment);
        $urlsegment = GlobalFunction::GenerateUrlSegment($item->Title);
        // Debug::show('<br>==========================================urlsegment '.$urlsegment.'-0');
        if($item->UrlSegment == $urlsegment.'-0'){
            $item->UrlSegment = $urlsegment;
            $item->write();
            
        }
    }

    
    
    //start lesson 14
    public function Link() {
        //return $this->RegionsPage()->Link('show/'.$this->ID);
        return $this->RegionsPage()->Link('show/'.$this->UrlSegment);
    }

    public function LinkingMode(){
        return Controller::curr()->getRequest()->param('ID') == $this->ID ? 'current' : 'link';
    }
    //end lesson 14

    //start lesson 19
    public function ArticlesLink() {
        $page = ArticleHolderPage::get()->first();
        if($page){
            return $page->Link('region/'.$this->UrlSegment);
        }
    }
    //end lesson 19
}
