<?php

class HomePage extends Page {

}

class HomePage_Controller extends Page_Controller {
    public function test(){
        die('it works');
    }

    public function LatestArticles($count){
        return ArticlePage::get()
                ->sort('Created', 'Desc')
                ->limit(3);
    }

    public function FeaturedProperties(){
        return PropertyData::get()
                ->filter(array(
                    'FeaturedOnHomePage' => true
                ))
                ->limit(6);
        
    }

}