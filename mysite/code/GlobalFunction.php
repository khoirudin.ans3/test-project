<?php
class GlobalFunction{
    
    public static function GenerateUrlSegment($Title){
        $urlnew = str_replace(' ', '-', $Title);
        $urlnew = strtolower($urlnew);
        return $urlnew;
    }

    public static function FindUrlSegmentCategory($urlsegment){
        $findurlsegment = ArticleCategoryData::get()->filter(array(
            'UrlSegment' => $urlsegment
        ))->first();
        return $findurlsegment;
    }

    public static function FindUrlSegmentRegion($urlsegment){
        $findurlsegment = RegionData::get()->filter(array(
            'UrlSegment' => $urlsegment
        ))->first();
        return $findurlsegment;
    }

    public static function FindUrlSegmentProperty($urlsegment){
        $findurlsegment = PropertyData::get()->filter(array(
            'UrlSegment' => $urlsegment
        ))->first();
        return $findurlsegment;
    }
    
    public static function GenerateWaNumber($nohp){
		$nohp = trim($nohp);
		$nohp = strip_tags($nohp);     
		$nohp = str_replace(" ","",$nohp);
		$nohp = str_replace("(","",$nohp);
		$nohp = str_replace(".","",$nohp);    
		if(!preg_match('/[^+0-9]/',trim($nohp))){
            if(substr($nohp, 0, 1)=='0'){
		        $nohp = '62'.substr($nohp, 1);
		    }
		}		
		return $nohp;
    }
}
