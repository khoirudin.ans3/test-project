<?php
class PropertySearchPage extends Page{

}

class PropertySearchPage_Controller extends Page_Controller{
    // private static $allowed_actions = array(
    //     'test',
    // );

    // public function test(){
    //     die('it works');
    // }

    public function index(SS_HTTPRequest $request){
        // $properties = PropertyData::get();
        $filters = ArrayList::create();
        
        $keywords = $request->getVar('Keywords');
        $arrival = $request->getVar('ArrivalDate');
        $bedrooms = $request->getVar('Bedrooms');
        $bathrooms = $request->getVar('Bathrooms');
        $minPrice = $request->getVar('MinPrice');
        $maxPrice = $request->getVar('MaxPrice');
        $type = $request->getVar('Type');
        $fasility = $request->getVar('Fasility');

        $sqlwhere = "";
        if($keywords){
            $sqlwhere = " AND p.Title Like '%$keywords%' ";
            $filters->push(ArrayData::create(array(
                'Label' => "Keywords: $keywords",
                'RemoveLink' => HTTP::setGetVar('Keywords', null)
            )));
        }
        if($arrival){
            $arrivalStamp = strtotime($arrival);
            $nightAdder = '+'.$request->getVar('Nights').' days';
            $startDate = date('Y-m-d', $arrivalStamp);
            $endDate = date('Y-m-d', strtotime($nightAdder, $arrivalStamp));
            $sqlwhere = " AND p.AvailableStart >= '$startDate' AND p.AvailableEnd >= '$endDate' ";
        }
        if($bedrooms){
            $sqlwhere = " $sqlwhere  AND p.Bedrooms >= '$bedrooms' ";
            $filters->push(ArrayData::create(array(
                'Label' => "Bedrooms: $bedrooms",
                'RemoveLink' => HTTP::setGetVar('Bedrooms', null)
            )));
        }
        if($bathrooms){
            $filters->push(ArrayData::create(array(
                'Label' => "Bathrooms: $bathrooms",
                'RemoveLink' => HTTP::setGetVar('Bathrooms', null)
            )));
            $sqlwhere = " $sqlwhere AND p.Bathrooms >= '$bathrooms' ";
        }
        if($minPrice){
            $sqlwhere = " $sqlwhere AND p.PricePerNight >= '$minPrice' ";
            $filters->push(ArrayData::create(array(
                'Label' => "Min. \$$minPrice",
                'RemoveLink' => HTTP::setGetVar('MinPrice', null)
            )));
        }
        if($maxPrice){
            $sqlwhere = " $sqlwhere AND p.PricePerNight <= '$maxPrice' ";
            $filters->push(ArrayData::create(array(
                'Label' => "Min. \$$maxPrice",
                'RemoveLink' => HTTP::setGetVar('MaxPrice', null)
            )));
        }
        if($type){
            $title = PropertyTypeData::get()->filter(array(
                    'ID' => $type
                ))->first();
                $filters->push(ArrayData::create(array(
                    'Label' => "Type $title->Title" ,
                    'RemoveLink' => HTTP::setGetVar('Type', null)
                )));
            $sqlwhere = " $sqlwhere AND p.TypeID = '$type' ";
        }
        if($fasility){
            $fasility = implode("','",$fasility);
            $sqlwhere = " $sqlwhere AND PropertyFasilityDataID IN ('$fasility') ";
        }
        $sqlquery = "SELECT p.ID as PropertyID , p.Title FROM PropertyData p LEFT JOIN PropertyData_Fasilities pf ON p.ID = pf.PropertyDataID WHERE p.FeaturedOnHomePage = 1 $sqlwhere Group by PropertyID";
        $query = DB::query($sqlquery) ;
        $newfasilitylist = new ArrayList();
        foreach($query as $q){
            $propertyFasility = PropertyData::get();
            $propertyid = $q['PropertyID'];
            $propertieslist = $propertyFasility->filter(array(
                'ID' => $propertyid
            ))->first();
			$newfasilitylist->push($propertieslist);        
        }
        $properties = $newfasilitylist;
        
        // if($keywords){
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "Keywords: '$keywords'",
        //         'RemoveLink' => HTTP::setGetVar('Keywords', null)
        //     )));
        //     $properties = $properties->filter(array(
        //         'Title:PartialMatch' => $keywords
        //     ));
        // }

        // if($arrival){
        //     $arrivalStamp = strtotime($arrival);
        //     $nightAdder = '+'.$request->getVar('Nights').' days';
        //     $startDate = date('Y-m-d', $arrivalStamp);
        //     $endDate = date('Y-m-d', strtotime($nightAdder, $arrivalStamp));

        //     $properties = $properties->filter(array(
        //         'AvailableStart:LessThanOrEqual' => $startDate,
        //         'AvailableEnd:GreaterThanOrEqual' => $endDate
        //     ));
        // }
        // if($bedrooms){
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "$bedrooms bedrooms'",
        //         'RemoveLink' => HTTP::setGetVar('Bedrooms', null)
        //     )));
        //     $properties = $properties->filter(array(
        //         'Bedrooms:GreaterThanorEqual' => $bedrooms
        //     ));
        // }

        // if($bathrooms){
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "$bathrooms bathrooms'",
        //         'RemoveLink' => HTTP::setGetVar('Bathrooms', null)
        //     )));
        //     $properties = $properties->filter(array(
        //         'Bathrooms:GreaterThanOrEqual' => $bathrooms
        //     ));
        // }

        // if($minPrice) {
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "Min. \$$minPrice",
        //         'RemoveLink' => HTTP::setGetVar('MinPrice', null)
        //     )));
		// 	$properties = $properties->filter(array(
		// 		'PricePerNight:GreaterThanOrEqual' => $minPrice
		// 	));
		// }

		// if($maxPrice) {
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "Max. \$$maxPrice",
        //         'RemoveLink' => HTTP::setGetVar('MaxPrice', null)
        //     )));
		// 	$properties = $properties->filter(array(
		// 		'PricePerNight:LessThanOrEqual' => $maxPrice
		// 	));
		// }

        // if($type) {
        //     $title = PropertyTypeData::get()->filter(array(
        //         'ID' => $type
        //     ))->first();
        //     $filters->push(ArrayData::create(array(
        //         'Label' => "Type $title->Title" ,
        //         'RemoveLink' => HTTP::setGetVar('Type', null)
        //     )));
		// 	$properties = $properties->filter(array(
		// 		'TypeID' => $type
		// 	));
		// }

        // $fasility = $request->getVar('Fasility');
        // if($fasility) {
        //     $fasility = implode("','",$fasility);
        //     $keyword = 'lux';
        //     $sqlkeyword = " p.Title Like '%$keyword%' AND ";
        //     $sqlfasility = " PropertyFasilityDataID IN ('$fasility') ";
        //     $sqlquery = "SELECT p.ID as PropertyID , p.Title FROM PropertyData p INNER JOIN 
        //     PropertyData_Fasilities pf ON p.ID = pf.PropertyDataID WHERE $sqlkeyword $sqlfasility Group by PropertyID";
        //     $query = DB::query($sqlquery) ;
        //     $newfasilitylist = new ArrayList();
        //     foreach($query as $q){
        //         $propertyFasility = PropertyData::get();
        //         $propertyid = $q['PropertyID'];
        //         $propertieslist = $propertyFasility->filter(array(
        //             'ID' => $propertyid
        //         ))->first();
		// 		$newfasilitylist->push($propertieslist);
                
        //     }

            //     Debug::show($newfasilitylist);
            //     Debug::show($propertyid);
            //     Debug::show($propertieslist);
            // die();
		// 	$properties = $newfasilitylist;
		// }

        $paginatedProperties = PaginatedList::create(
            $properties,
            $request
        )->setPageLength(3)
         ->setPaginationGetVar('s');

        $data = array(
            'Results' => $paginatedProperties,
            'ActiveFilters' => $filters
        );

        // Debug::show($data);
        // die();

        if($request->isAjax()){
            return $this->customise($data)
                        ->renderWith('PropertySearchResults');
        }

        return $data;
    }

    public function PropertySearchForm() {
        $nights = array ();
        foreach(range(1,14) as $i){
            $nights[$i] = "$i night" . (($i > 1) ? 's' : '');
        }
        $prices = array();
        foreach(range(100, 10000, 50) as $i){
            $prices[$i] = '$'.$i;
        }


        $form = Form::create(
            $this, 'PropertySearchForm',
            FieldList::create(
                TextField::create('Keywords')
                    ->setAttribute('placeholder', 'City, State, Country, etc...')
                    ->addExtraClass('form-control'),
                TextField::create('ArrivalDate', 'Arrival on...')
                    ->setAttribute('data-datepicker', true)
                    ->setAttribute('data-date-format', 'DD-MM-YYYY')
                    ->addExtraClass('form-control'),
                DropdownField::create('Nights', 'Stay for...')
                    ->setSource($nights)
                    ->addExtraClass('form-control'),
                DropdownField::create('Bedrooms')
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),
                DropdownField::create('Bathrooms')
                    ->setSource(ArrayLib::valuekey(range(1,5)))
                    ->addExtraClass('form-control'),
                DropdownFIeld::create('MinPrice', 'Min. price')
                    ->setEmptyString('-- any --')
                    ->setSource($prices)
                    ->addExtraClass('form-control'),
                DropdownFIeld::create('MaxPrice', 'Max. price')
                    ->setEmptyString('-- any --')
                    ->setSource($prices)
                    ->addExtraClass('form-control'),
                DropdownFIeld::create('Type', 'Type')
                    ->setEmptyString('-- any --')
                    ->setSource(
                        PropertyTypeData::get()->map('ID', 'Title')
                    )
                    ->addExtraClass('form-control'),
                CheckboxSetField::create('Fasility', 'Fasility')
                    ->setSource(
                        PropertyFasilityData::get()->map('ID', 'Title')
                    )
                    ->addExtraClass('form-check')
            ),
            FieldList::create(
                FormAction::create('doPropertySearch','Search')
                    ->addExtraClass('btn-lg btn-fullcolor')
            )
        );

        $form->setFormMethod('GET')
            ->setFormAction($this->Link())
            ->disableSecurityToken()
            ->loadDataFrom($this->request->getVars());

            // var_dump($this->request->getVars());
            // die();

        return $form;
    }
}

?>