<?php
class ArticleTagData extends DataObject{
    private static $db = array(
        'Title' => 'Varchar',
        'UrlSegment' => 'Varchar'
    );

    private static $has_one = array(
        'ArticleHolderPage' => 'ArticleHolderPage',
    );

    private static $belongs_many_many = array(
        'Articles' => 'ArticlePage',
    );

    public function getCMSFields(){
        return Fieldlist::create(
            TextField::create('Title')
        );
    }

}
?>