<?php
class AgentData extends DataObject{
    private static $db = array(
        'Name' => 'Varchar',
        'Address' => 'Varchar',
        'AboutUs' => 'Text',
        'NoHp' => 'Varchar'
    );

    private static $has_one = array(
        'ProfilePicture' => 'Image'
    );

    private static $has_many = array(
        'Properties' => 'PropertyData'
    );

    private static $summary_fields = array(
        'GridThumbnail' => 'Profile Picture',
        'Name' => 'Name',
        'Address' => 'Address',
        'AboutUs' => 'About Us',
        'NoHp' => 'No HP'
    );

    public function getGridThumbnail() {
        if($this->ProfilePicture()->exists()) {
            return $this->ProfilePicture()->SetWidth(100);
        }

        return "(no image)";
    }

    public function getCMSFields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', array(
            TextField::create('Name'),
            TextField::create('Address'),
            TextareaField::create('AboutUs', 'About us'),
            TextField::create('NoHp', 'No HP')
        ));

        $fields->addFieldsToTab('Root.Photos', $upload = UploadField::create(
            'ProfilePicture', 'Profile Picture'
        ));

        $upload->getValidator()->setAllowedExtensions(array(
            'png', 'jpg', 'jpeg', 'gif'
        ));

        $upload->setFolderName('agent-photos');

        return $fields;
    }
}
?>