<?php
class PropertyData extends DataObject{
    private static $db = array(
        'Title' => 'Varchar',
        'PricePerNight' => 'Currency',
        'Bedrooms' => 'Int',
        'Bathrooms' => 'Int',
        'FeaturedOnHomePage' => 'Boolean',
        'Address' => 'Varchar',
        'NoAddress' => 'Varchar',
        'LuasTanah' => 'Int',
        'LuasBangunan' => 'Int',
        'Transaksi' => 'Varchar',
        //lesson 15
        'Description' => 'HTMLText',
        'AvailableStart' => 'Date',
        'AvailableEnd' => 'Date',
        'UrlSegment' => 'varchar'
    );

    private static $has_one = array(
        'Region' => 'RegionData',
        'PrimaryPhoto' => 'Image',
        'Type' => 'PropertyTypeData',
        'Agent' => 'AgentData',
        'HomePage' => 'HomePage'
    );

    private static $many_many = array(
        'Fasilities' => 'PropertyFasilityData'
    );

    private static $summary_fields = array(
        'Title' => 'Title',
        'Region.Title' => 'Region',
        'Type.Title' => 'Type',
        'PricePerNight.Nice' => 'Price',
        'FeaturedOnHomePage.Nice' => 'Featured?',
        'Transaksi' => 'Transaksi',
        'Address' => 'Address',
        'Agent.Name' => 'Agent Name'
    );

    // private static $searchable_fields = array(
    //     'Title',
    //     'Region.Title',
    //     'FeaturedOnHomePage'
    // );

    public function searchableFields(){
        return array(
            'Title' => array(
                'filter' => 'PartialMatchFilter',
                'title' => 'Title',
                'field' => 'TextField'
            ),
            'RegionID' => array(
                'filter' => 'ExactMatchFilter',
                'title' => 'Region',
                'field' => DropdownField::create('RegionID')
                    ->setSource(
                        RegionData::get()->map('ID', 'Title')
                    )
                    ->setEmptyString('-- Any Region --')
            ),
            'FeaturedOnHomePage' => array(
                'filter' => 'ExactMatchFilter',
                'title' => 'Only featured'
            )
        );
    }

    public function getCMSFields(){
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main',array(
            TextField::create('Title'),
            //startlesson16
            HtmlEditorField::create('Description'),
            //endlesson16
            CurrencyField::create('PricePerNight','Price (per night)'),
            DropdownFIeld::create('TypeID', 'Type')
                ->setSource(PropertyTypeData::get()->map('ID', 'Title')),
            DropdownField::create('Bedrooms')
                ->setSource(ArrayLib::valuekey(range(1,10))),
            DropdownField::create('Bathrooms')
                ->setSource(ArrayLib::valuekey(range(1,10))),
            DropdownFIeld::create('RegionID', 'Region')
                ->setSource(RegionData::get()->map('ID', 'Title')),
            TextField::create('Address'),
            TextField::create('NoAddress'),
            TextField::create('LuasTanah', 'Luas Tanah (m2)'),
            TextField::create('LuasBangunan', 'Luas Bangunan (m2)'),
            DropdownField::create(
                'Transaksi',
                'Transaksi',
                array(
                    'Jual'=>'Jual',
                    'Sewa'=>'Sewa')
                ),
            CheckboxField::create('FeaturedOnHomePage', 'Feature on homepage'),

            DropdownFIeld::create('AgentID', 'Agent')->setSource(AgentData::get()->map('ID', 'Title'))
            
        ));

        $fields->addFieldToTab('Root.Photos', $upload = UploadFIeld::create(
            'PrimaryPhoto', 'Primary Photo'
        ));

        $fields->addFieldToTab('Root.Fasilities', CheckboxSetField::create(
            'Fasilities',
            'Selected fasilities'
            )->setSource(PropertyFasilityData::get()->map('ID', 'Title'))
        );

        $upload->getValidator()->setAllowedExtensions(array(
            'png', 'jpg', 'jpeg', 'gif'
        ));

        $upload->setFolderName('property-photos');

        return $fields;
    }

    public function onBeforeWrite(){
        parent::onBeforeWrite();
        $item = $this;
        $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
        $findurlsegment = GlobalFunction::FindUrlSegmentProperty($urlsegment);
        // Debug::show($findurlsegment);
        // $findurlsegment = true;
        if($findurlsegment){
            // Debug::show('==========================================ada');
            $i=0;
            while($findurlsegment){
                // $urlsegmenextra = $findurlsegment->UrlSegment.'-'.$i++;
                // $urlsegment = GlobalFunction::GenerateUrlSegment($findurlsegment);
                
                $urlsegmentdb = GlobalFunction::GenerateUrlSegment($this->Title.' '.$i);
                $findurlsegment = GlobalFunction::FindUrlSegmentProperty($urlsegmentdb);
                if(!$findurlsegment){
                    // Debug::show('==========================================loop');
                    $item->UrlSegment = $urlsegmentdb;
                    //return $findurlsegment;
                    // return true;
                }
                $i++;
            }
        }
        else{
            // Debug::show('==========================================gada');
            $urlsegment = GlobalFunction::GenerateUrlSegment($this->Title);
            $item->UrlSegment = $urlsegment;
            // Debug::show('==========================================gada  '.$item->UrlSegment);
            // return true;
        }
    }


    public function onAfterWrite(){
        parent::onAfterWrite();
        // use the find() method to look up the relation
        $property = PropertyData::get()->find('ID', $this->ID);
        $urlsegment = GlobalFunction::GenerateUrlSegment($property->Title);

        if($property->UrlSegment == $urlsegment.'-0'){
            $property->UrlSegment = $urlsegment;
            $property->write();
            
        }
        // $urlnew = str_replace(' ', '-', $property->Title);
        // $urlnew = strtolower($urlnew);
        // $countproperty = PropertyData::get()->filter(array(
        //     'Title' => $property->Title
        // ))->count();

        // if($countproperty == 1){
        //     $property->UrlSegment = $urlnew;
        //     $property->write();
        // }
        // if($countproperty>=2){
        //     $i = 0;
        //     while($i <= $countproperty){
        //         if ($i == $countproperty){
        //             $property->UrlSegment = $urlnew.'-'.$i++;
        //             $property->write();
        //         }
        //         $i++;
        //     }
        // }
    }

    public function Link() {
        //return $this->RegionsPage()->Link('show/'.$this->ID);
        // $link = Director::AbsoluteBaseURL();
        //dr ferry
        //$link = PropertyPage::get()->first()->Link('detail/'.$this->UrlSegment);
        // return $link;
        //end dr ferry
        return $this->HomePage()->Link('/property/detail/'.$this->UrlSegment);

        
        // return $this->HomePage()->Link('property-detail/'.$this->UrlSegment);
    }
}
?>