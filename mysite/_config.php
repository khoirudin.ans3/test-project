<?php

global $project;
$project = 'mysite';

global $databaseConfig;
$databaseConfig = array(
	'type' => 'MySQLDatabase',
	'server' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'SS_mysite',
	'path' => ''
);
Security::setDefaultAdmin('admin', '1234');
// Set the site locale
i18n::set_locale('en_US');
Director::set_environment_type('dev');

if(Director::isTest()){
	SS_Log::add_writer(new SS_LogFileWriter('../silverstripe-error-warnings.log'), SS_Log::Warn, '<=');
	SS_Log::add_writer(new SS_LogFileWriter('../silverstripe-errors.log'), SS_Lang::ERR);
}

if(Director::isLive()){
	SS_Log::add_writer(new SS_LogEmailWriter('iyeiscool@gmail.com'), SS_Log::ERR);
}
